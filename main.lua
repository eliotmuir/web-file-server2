local Server = require("HTTP.HTTPproxyServer")
local FTP = require("htftp")

local Configs = component.fields()
local RootDir = Configs.RootDir
local Settings = {
   port = Configs.Port,
   ssl = {
      cert = Configs.Cert,
      key = Configs.PrivateKey
   }
}

function main(Data)
   iguana.log("Received request:\n" .. Data:sub(1,1024))
   local Request = Server.parseRequest(Data)
   local Args = Server.parseRequestUri(Request.path)
   local Response = FTP.dir_html(Args.path, Args.sort, RootDir)
   Server.respond(Response)
   -- truncate body for logging
   Response.body = Response.body:sub(1, 1024)
   iguana.log("Sent response:\n" .. json.serialize{data=Response})
end

Server.start(Settings)
